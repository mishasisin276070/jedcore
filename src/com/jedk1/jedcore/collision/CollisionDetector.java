package com.jedk1.jedcore.collision;

import com.projectkorra.projectkorra.GeneralMethods;
import com.projectkorra.projectkorra.ability.ElementalAbility;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.*;

public class CollisionDetector {
    public static boolean checkEntityCollisions(Player player, Collider collider, CollisionCallback function) {
        return checkEntityCollisions(player, collider, function, true);
    }

    public static boolean checkEntityCollisions(Player player, Collider collider, CollisionCallback callback, boolean livingOnly) {
        return checkEntityCollisions(player.getWorld(), player, collider, callback, livingOnly);
    }
    // Checks a collider to see if it's hitting any entities near it.
    // Calls the CollisionCallback when hitting a target.
    // Returns true if it hits a target.
    public static boolean checkEntityCollisions(World world, Player player, Collider collider, CollisionCallback callback, boolean livingOnly) {
        // This is used to increase the lookup volume for nearby entities.
        // Entity locations can be out of the collider volume while still intersecting.
        final double ExtentBuffer = 4.0;

        // Create the extent vector to use as size of bounding box to find nearby entities.
        Vector extent = collider.getHalfExtents().add(new Vector(ExtentBuffer, ExtentBuffer, ExtentBuffer));

        Vector pos = collider.getPosition();
        Location location = new Location(world, pos.getX(), pos.getY(), pos.getZ());

        boolean hit = false;

        for (Entity entity : location.getWorld().getNearbyEntities(location, extent.getX(), extent.getY(), extent.getZ())) {
            if (player != null && entity == player) continue;
            if (entity instanceof ArmorStand) continue;

            if (entity instanceof Player && ((Player) entity).getGameMode().equals(GameMode.SPECTATOR)) {
                continue;
            }

            if (livingOnly && !(entity instanceof LivingEntity)) {
                continue;
            }

            AABB entityBounds = new AABB(entity).at(entity.getLocation());

            if (collider.intersects(entityBounds)) {
                if (callback.onCollision(entity)) {
                    return true;
                }

                hit = true;
            }
        }

        return hit;
    }

    // Checks if the entity is on the ground. Uses NMS bounding boxes for accuracy.
    public static boolean isOnGround(Entity entity) {
        return entity.isOnGround();
    }

    public static double distanceAboveGround(Player player) {
        Location l = player.getLocation().clone();
        while (l.getBlock() != null && l.getBlockY() > 1 && !GeneralMethods.isSolid(l.getBlock())) {
            l.add(0, -0.1, 0);
        }
        return player.getLocation().getY() - l.getY();
    }

    public interface CollisionCallback {
        // return true to break out of the loop
        boolean onCollision(Entity e);
    }
}
