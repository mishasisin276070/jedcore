package com.jedk1.jedcore.ability.gift;

import com.projectkorra.projectkorra.Element;
import com.projectkorra.projectkorra.ProjectKorra;
import com.projectkorra.projectkorra.ability.ElementalAbility;
import org.bukkit.entity.Player;

public abstract class GiftAbility extends ElementalAbility {
    public static final Element GIFT = new Element("Gift", null, ProjectKorra.plugin);

    public GiftAbility(Player player) {
        super(player);
    }

    @Override
    public Element getElement() {
        return GIFT;
    }
}
